<?php 
include_once("connection.php");

/**
 * Class objectivesModel
 *
 */
class objectivesModel
{

	public function getAllObj()
	{
		$query = "SELECT * FROM objectives ORDER BY obj_date_ini ASC";
		$res = executeQuery($query);
		
		return $res;
	}
	
	public function getObj($obj_uid)
	{
		$query = "SELECT * FROM objectives WHERE obj_uid = '" . $obj_uid . "'";
		$res = executeQuery($query);	

		return $res;
	}
	
	public function createObj($arrayData)
	{
		$arrayData = array_change_key_case($arrayData, CASE_UPPER);		//mayusculas
		$form = $arrayData;
		$data = array();
		
		// A uniqid, like: 002515wnsm5ss95
		$data['OBJ_UID'] = uniqid('00');
		
		if($form['OBJ_TITLE'] == ''){
			throw new \Exception("Objective tittle needed.");
		} else {
		    $data['OBJ_TITLE'] = $form['OBJ_TITLE'];
		}
		
		$data['OBJ_DESCRIPTION'] = $form['OBJ_DESCRIPTION'];
		
		if($form['OBJ_DATE_INI'] == ''){
			throw new \Exception("An start objective date is needed.");
		} else {
			$data['OBJ_DATE_INI'] = $form['OBJ_DATE_INI'];
		}
		
		if($form['OBJ_DATE_END'] == ''){
			throw new \Exception("An ending objective date is needed.");
		} else {
			$data['OBJ_DATE_END'] = $form['OBJ_DATE_END'];
		}
		
		if($form['OBJ_DAYS'] == ''){
			throw new \Exception("The days to do this objective are needed.");
		} else {
			$data['OBJ_DAYS'] = $form['OBJ_DAYS'];
		}
				
		if($form['OBJ_DAYTIME_INI'] == ''){
			throw new \Exception("An start time to do thid objective is needed.");
		} else {
			$data['OBJ_DAYTIME_INI'] = $form['OBJ_DAYTIME_INI'];
		}
		
		if($form['OBJ_DAYTIME_END'] == ''){
			throw new \Exception("An ending time to do thid objective is needed.");
		} else {
			$data['OBJ_DAYTIME_END'] = $form['OBJ_DAYTIME_END'];
		}
		
		if($form['OBJ_ALARM_STATUS'] == ''){
			$data['OBJ_ALARM_STATUS'] = "0";
		} else {
			$data['OBJ_ALARM_STATUS'] = $form['OBJ_ALARM_STATUS'];
		}
		
	    if( $data['OBJ_ALARM_STATUS'] == 1){
	    	if($form['OBJ_ALARM_TIME'] == ''){  //Alarma Date
	    		throw new \Exception("The alarm time before the objective is needed.");
	    	} else {
	    		$data['OBJ_ALARM_TIME'] = $form['OBJ_ALARM_TIME'];
	    	}
	    } else {
	    	$data['OBJ_ALARM_TIME'] = "";
	    }		
		
	    if($form['USR_UID'] == ''){
	    	throw new \Exception("The user uid for the objective is needed.");
	    } else {
	    	$data['USR_UID'] = $form['USR_UID'];
	    }
		
		$query = "INSERT INTO objectives (obj_uid, obj_title, obj_description, obj_date_ini, obj_date_end, obj_days, obj_daytime_ini, obj_daytime_end, obj_alarm_status, obj_alarm_time, usr_uid) 
		VALUES ('" . $data['OBJ_UID'] . "', '" . $data['OBJ_TITLE'] . "', '" . $data['OBJ_DESCRIPTION'] . "', '" . $data['OBJ_DATE_INI'] . "', '" . 
		$data['OBJ_DATE_END'] . "', '" . $data['OBJ_DAYS'] . "', '" . $data['OBJ_DAYTIME_INI'] . "', '" . $data['OBJ_DAYTIME_END'] . "', '" . $data['OBJ_ALARM_STATUS'] . "', '" . $data['OBJ_ALARM_TIME'] . "', '" . $data['USR_UID'] . "')"; 
		
		//print_r($query);
		//die('hello');
		$res = executeQuery($query);
		
		return $res;
	}

	public function deleteObj($obj_uid)
	{
		if($obj_uid == ""){
			throw new \Exception("The obj_uid is needed.");
		} else {
			$query = "DELETE FROM objectives WHERE obj_uid = '" . $obj_uid . "'";
			$res = executeQuery($query);
		}
		return $res;
	}
	
	
	public function getUserObjectives($usr_uid)
	{
		if($usr_uid == ""){
			throw new \Exception("The usr_uid is needed to list his objectives.");
		} else {
			$query = "SELECT * FROM objectives WHERE usr_uid = '" . $usr_uid . "'";
			$res = executeQuery($query);
		}
		
		$state = array();
		for($i=0; $i<count($res); $i++){
			$obj_uid = $res[$i]->obj_uid;
			$state[$i] = $this->getObjState($obj_uid);
			
			$res[$i]->progress= $state[$i];
		}

		return $res;
	}
	
	//GET
	public function getObjState($obj_uid)
	{
		if($obj_uid == ""){
			throw new \Exception("The obj_uid is needed to get the objective state.");
		} else {
			$query = "SELECT obj_date_ini, obj_date_end, obj_days, obj_daytime_ini, obj_daytime_end, obj_status FROM objectives WHERE obj_uid = '" . $obj_uid . "'";
			$resquery = executeQuery($query);	

			if(count($resquery) == 0){
				throw new \Exception("The objective dosen't exist.");
			}
			
			//getting the Total Weeks of the objective
			$date_ini_obj = $resquery[0]->obj_date_ini;
			$date_end_obj = $resquery[0]->obj_date_end;
			
			$seconds = strtotime($date_ini_obj) - strtotime($date_end_obj); // Obtenemos los segundos entre esas dos fechas
			$seconds = abs($seconds); //en caso de errores
			
			$weeks = floor($seconds / 604800); //Obtenemos las semanas entre esas fechas.
			
			//getting the tot days per week
			$days = explode("-", $resquery[0]->obj_days);
			$totDaysxWeek = count($days);
			
			//ending data
			$totDaysObj = $weeks * $totDaysxWeek;
			$actualStatus = $resquery[0]->obj_status;	
			
			$responseTotStatus = ($actualStatus * 100)/$totDaysObj;
			
			return round($responseTotStatus);
		}		
	}
	
	//PUT
	//&day_state is a 1 / 0 state
	public function registerObjectiveState($obj_uid="", $day_state="")
	{
		print_r($obj_uid, $day_state);
		die();
		if($obj_uid == ""){
			throw new \Exception("The obj_uid is needed to register the objective state.");
		} else {
			$query = "SELECT obj_status FROM objectives WHERE obj_uid = '" . $obj_uid . "'";
			$resquery = executeQuery($query);
			
			if(count($resquery) == 0){
				throw new \Exception("The objective dosen't exist.");
			}
			
			$actualStatus = $resquery[0]->obj_status;
			$actualStatus = (int)$actualStatus;
				
			if($day_state == 1){
				$newStatus = $actualStatus + 1;
	
				$query = "UPDATE objectives SET obj_status = '" . $newStatus . "' WHERE obj_uid = '" . $obj_uid . "'";
				$resquery = executeQuery($query);
					
				return $resquery;				
			} elseif($day_state == 0){					
				$newStatus = $actualStatus;
					
				$query = "UPDATE objectives SET obj_status = '" . $newStatus . "' WHERE obj_uid = '" . $obj_uid . "'";
				$resquery = executeQuery($query);
					
				return $resquery;
			} else {
				throw new \Exception("The state is invalid, it has to be 1 to complete and 0 to incomplete.");
			}	
		}
	}
	
	public function updateObj($obj_uid, $arrayData)
	{
		$arrayData = array_change_key_case($arrayData, CASE_UPPER);		//mayusculas
		$form = $arrayData;
		$data = array();
		
		if($form['OBJ_TITLE'] == ''){
			throw new \Exception("Objective tittle needed.");
		} else {
			$data['OBJ_TITLE'] = $form['OBJ_TITLE'];
		}
		
		$data['OBJ_DESCRIPTION'] = $form['OBJ_DESCRIPTION'];
		
		if($form['OBJ_DATE_END'] == ''){
			throw new \Exception("An ending objective date is needed.");
		} else {
			$data['OBJ_DATE_END'] = $form['OBJ_DATE_END'];
		}
	
		if($form['OBJ_DAYS'] == ''){
			throw new \Exception("The days to do this objective are needed.");
		} else {
			$data['OBJ_DAYS'] = $form['OBJ_DAYS'];
		}
	
		if($form['OBJ_DAYTIME_INI'] == ''){
			throw new \Exception("An start time to do thid objective is needed.");
		} else {
			$data['OBJ_DAYTIME_INI'] = $form['OBJ_DAYTIME_INI'];
		}
	
		if($form['OBJ_DAYTIME_END'] == ''){
			throw new \Exception("An ending time to do thid objective is needed.");
		} else {
			$data['OBJ_DAYTIME_END'] = $form['OBJ_DAYTIME_END'];
		}
	
		if($form['OBJ_ALARM_STATUS'] == ''){
			$data['OBJ_ALARM_STATUS'] = "0";
		} else {
			$data['OBJ_ALARM_STATUS'] = $form['OBJ_ALARM_STATUS'];
		}
	
		if( $data['OBJ_ALARM_STATUS'] == 1){
			if($form['OBJ_ALARM_TIME'] == ''){  //Alarma Date
				throw new \Exception("The alarm time before the objective is needed.");
			} else {
				$data['OBJ_ALARM_TIME'] = $form['OBJ_ALARM_TIME'];
			}
		} else {
			$data['OBJ_ALARM_TIME'] = "";
		}

		print_r($data);
		
		$query = "UPDATE objectives SET obj_title='".$data['OBJ_TITLE']."', obj_description ='".$data['OBJ_DESCRIPTION']."',
		obj_date_end= '".$data['OBJ_DATE_END']."', obj_days='".$data['OBJ_DAYS']."',
		obj_daytime_ini='".$data['OBJ_DAYTIME_INI']."', obj_daytime_end= '".$data['OBJ_DATE_END']."',
		obj_alarm_status='".$data['OBJ_ALARM_STATUS']."', obj_alarm_time='".$data['OBJ_ALARM_TIME']."' WHERE obj_uid= '".$obj_uid."'";

		$resquery = executeQuery($query);
		
		return($resquery);
	}
	
}
?>

