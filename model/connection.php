<?php 
//$conexion = mysql_connect("localhost", "root", "");
//mysql_select_db("waimbook", $conexion);

function executeQuery ($sQuery)
{   
	$sQuery = trim($sQuery);
	if ($sQuery == '') {
		return false;
	}

	// New Connection
	$db = new mysqli('localhost','root','','waimbook');
	$resp = array();

	// Check for errors
	if (mysqli_connect_errno()) {
		echo mysqli_connect_error();
	}

	// 1st Query
	$result = $db->query($sQuery);

	if ($result) {
		if (preg_match("/^SELECT/",$sQuery)) {
			while ($row = $result->fetch_object()){
				$resp[] = $row;
			}
			$result->close();
			$db->more_results();
		}
	} else {
		self::pr('--------<br/>La consulta SQL... <br/>--------<br/>' . $sQuery . '<br/>--------<br/>contiene el error...'.
				'<br/>--------<br/>' . $db->error . '<br/>--------');
	}

	// Close connection
	$db->close();
	return $resp;
}


?>
