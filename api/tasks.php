<?php
use Luracast\Restler\RestException;
include_once("model/tasks.php");

/**
 * Class Tasks
 *
 */
class Tasks
{    
    /**
     * @url GET /:tsk_uid
     *
     * @param string $tsk_uid
     */
    public function getTask($tsk_uid)
    {
    	try{
    		$tasks = new tasksModel();
    		$resp = $tasks->getTask($tsk_uid);
    		return $resp;
    	} catch (\Exception $e){
    		return $e->getMessage();
    	}
    }
    
    
    /**
     * @url GET /objective/:obj_uid
     *
     * @param string $obj_uid
     */
    public function getObjTask($obj_uid)
    {
        try{
            $tasks = new tasksModel();
            $resp = $tasks->getObjTasks($obj_uid);
            //$foo = array('bar' => 'baz');
            return $resp;
        } catch (\Exception $e){
            //throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
            return $e->getMessage();
        }
    }
    
    /**
     * @url POST
     *
     * @param array $request_data
     *
     * @status 201
     */
    public function postTask($request_data)
    {
    	try {
    		$task = new taskModel();
    		$arrayData = $task->createTask($request_data);
    		$response = $arrayData;
    		return $response;
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    /**
     * @url PUT /:tsk_uid
     *
     * @param string $tsk_uid 
     * @param array  $request_data
     */
    public function putTask($tsk_uid, $request_data)
    {
    	try {
    		//$userLoggedUid = $this->getUserId();
    		$task = new taskModel();
    		//$arrayData = $user->update($usr_uid, $request_data, $userLoggedUid);
    		$arrayData = $task->update($tsk_uid, $request_data);
    		$response = $arrayData;
    		return $response;
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    /**
     * @url DELETE /:tsk_uid
     *
     * @param string $tsk_uid
     */
    public function deleteTask($tsk_uid)
    {
    	try {
    		$task = new taskModel();
    		$task->deleteTask($tsk_uid);
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }

}