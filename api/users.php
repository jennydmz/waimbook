<?php
use Luracast\Restler\RestException;
include_once("model/users.php");
header('Access-Control-Allow-Origin: *');

/**
 * Class Users
 *
 */
class Users
{
	/**
	 * @url GET
	 */
    public function getUsers()
    {      
        try{
        	$users = new usersModel();
        	$resp = $users->getAll();
        	return $resp;
        } catch (\Exception $e){
        	//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
        	return $e->getMessage();
        }
    }
    
    /**
     * @url GET /:usr_uid
     *
     * @param string $usr_uid {@min 15}{@max 32}
     */
    public function getUser($usr_uid)
    {
    	try{
    		$users = new usersModel();
    		$resp = $users->getUser($usr_uid);
    		//$foo = array('bar' => 'baz');
    		return $resp;
    	} catch (\Exception $e){
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    /**
     * @url POST
     *
     * @param array $request_data
     *
     * @status 201
     */
    public function postUser($request_data)
    {
    	try {
    		$user = new usersModel();
    		$arrayData = $user->createUser($request_data);
    		$response = $arrayData;
    		return $response;
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    /**
     * @url PUT /:usr_uid
     *
     * @param string $usr_uid      {@min 15}{@max 32}
     * @param array  $request_data
     */
    public function putUser($usr_uid, $request_data)
    {
    	try {
    		//$userLoggedUid = $this->getUserId();
    		$user = new usersModel();
    		//$arrayData = $user->update($usr_uid, $request_data, $userLoggedUid);
    		$arrayData = $user->update($usr_uid, $request_data);
    		$response = $arrayData;
    		return $response;
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    /**
     * @url DELETE /:usr_uid
     *
     * @param string $usr_uid {@min 15}{@max 32}
     */
    public function deleteUser($usr_uid)
    {
    	try {
    		$user = new usersModel();
    		$user->deleteUser($usr_uid);
    	} catch (\Exception $e) {
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
    
    
    /**
     * @url GET /:usr_username/:usr_password
     *
     * @param string usr_username 
     * @param string usr_password {@min 6}{@max 32} 
     */
    public function getlogin($usr_username, $usr_password)
    {
    	try{
    		$users = new usersModel();
    		$resp = $users->login($usr_username, $usr_password);
    		//$foo = array('bar' => 'baz');
    		return $resp;
    	} catch (\Exception $e){
    		//throw (new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage()));
    		return $e->getMessage();
    	}
    }
}